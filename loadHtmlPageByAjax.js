m.showBlankPage = function() {
  $.ajax({
    url: '/repsNGen/r_mainPage/blankPage.php',
    dataType: 'html',
    beforeSend: function () {w.showOverlay();},
    complete: function (data) {
      $(m.div).html(data.responseText);
      w.hideOverlay();
    }
  });
};
